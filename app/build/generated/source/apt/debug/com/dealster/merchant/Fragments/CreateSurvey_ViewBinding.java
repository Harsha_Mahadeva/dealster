// Generated code from Butter Knife. Do not modify!
package com.dealster.merchant.Fragments;

import android.annotation.SuppressLint;
import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.dealster.merchant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateSurvey_ViewBinding implements Unbinder {
  private CreateSurvey target;

  private View view2131230851;

  private View view2131231026;

  private View view2131231024;

  private View view2131230947;

  private View view2131230915;

  private View view2131230916;

  private View view2131230917;

  private View view2131230918;

  private View view2131230919;

  private View view2131231035;

  private View view2131230905;

  private View view2131230945;

  private View view2131230892;

  private View view2131231019;

  @UiThread
  @SuppressLint("ClickableViewAccessibility")
  public CreateSurvey_ViewBinding(final CreateSurvey target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.from_date, "field 'fromButton' and method 'datePicker'");
    target.fromButton = Utils.castView(view, R.id.from_date, "field 'fromButton'", Button.class);
    view2131230851 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.datePicker(Utils.castParam(p0, "doClick", 0, "datePicker", 0, Button.class));
      }
    });
    target.optionLayout = Utils.findRequiredViewAsType(source, R.id.option_layout, "field 'optionLayout'", LinearLayout.class);
    target.ratingLayout = Utils.findRequiredViewAsType(source, R.id.rating_question_layout, "field 'ratingLayout'", LinearLayout.class);
    target.textLayout = Utils.findRequiredViewAsType(source, R.id.text_question_layout, "field 'textLayout'", LinearLayout.class);
    target.terms = Utils.findRequiredViewAsType(source, R.id.terms, "field 'terms'", EditText.class);
    view = Utils.findRequiredView(source, R.id.text_question, "field 'textQuestion' and method 'onClear'");
    target.textQuestion = Utils.castView(view, R.id.text_question, "field 'textQuestion'", EditText.class);
    view2131231026 = view;
    view.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View p0, MotionEvent p1) {
        return target.onClear(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.text_block, "field 'textBlock' and method 'onClear'");
    target.textBlock = Utils.castView(view, R.id.text_block, "field 'textBlock'", EditText.class);
    view2131231024 = view;
    view.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View p0, MotionEvent p1) {
        return target.onClear(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.rating_question, "field 'ratingQuestion' and method 'onClear'");
    target.ratingQuestion = Utils.castView(view, R.id.rating_question, "field 'ratingQuestion'", EditText.class);
    view2131230947 = view;
    view.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View p0, MotionEvent p1) {
        return target.onClear(p0, p1);
      }
    });
    view = Utils.findRequiredView(source, R.id.option1, "field 'option1' and method 'onTouch'");
    target.option1 = Utils.castView(view, R.id.option1, "field 'option1'", EditText.class);
    view2131230915 = view;
    view.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View p0, MotionEvent p1) {
        return target.onTouch(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.option2, "field 'option2' and method 'onTouch'");
    target.option2 = Utils.castView(view, R.id.option2, "field 'option2'", EditText.class);
    view2131230916 = view;
    view.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View p0, MotionEvent p1) {
        return target.onTouch(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.option3, "field 'option3' and method 'onTouch'");
    target.option3 = Utils.castView(view, R.id.option3, "field 'option3'", EditText.class);
    view2131230917 = view;
    view.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View p0, MotionEvent p1) {
        return target.onTouch(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.option4, "field 'option4' and method 'onTouch'");
    target.option4 = Utils.castView(view, R.id.option4, "field 'option4'", EditText.class);
    view2131230918 = view;
    view.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View p0, MotionEvent p1) {
        return target.onTouch(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.option5, "field 'option5' and method 'onTouch'");
    target.option5 = Utils.castView(view, R.id.option5, "field 'option5'", EditText.class);
    view2131230919 = view;
    view.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View p0, MotionEvent p1) {
        return target.onTouch(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.to_date, "method 'datePicker'");
    view2131231035 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.datePicker(Utils.castParam(p0, "doClick", 0, "datePicker", 0, Button.class));
      }
    });
    view = Utils.findRequiredView(source, R.id.next, "method 'next'");
    view2131230905 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.next();
      }
    });
    view = Utils.findRequiredView(source, R.id.rating, "method 'addScreen'");
    view2131230945 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.addScreen(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.multiple_choice, "method 'addScreen'");
    view2131230892 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.addScreen(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.text, "method 'addScreen'");
    view2131231019 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.addScreen(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateSurvey target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.fromButton = null;
    target.optionLayout = null;
    target.ratingLayout = null;
    target.textLayout = null;
    target.terms = null;
    target.textQuestion = null;
    target.textBlock = null;
    target.ratingQuestion = null;
    target.option1 = null;
    target.option2 = null;
    target.option3 = null;
    target.option4 = null;
    target.option5 = null;

    view2131230851.setOnClickListener(null);
    view2131230851 = null;
    view2131231026.setOnTouchListener(null);
    view2131231026 = null;
    view2131231024.setOnTouchListener(null);
    view2131231024 = null;
    view2131230947.setOnTouchListener(null);
    view2131230947 = null;
    view2131230915.setOnTouchListener(null);
    view2131230915 = null;
    view2131230916.setOnTouchListener(null);
    view2131230916 = null;
    view2131230917.setOnTouchListener(null);
    view2131230917 = null;
    view2131230918.setOnTouchListener(null);
    view2131230918 = null;
    view2131230919.setOnTouchListener(null);
    view2131230919 = null;
    view2131231035.setOnClickListener(null);
    view2131231035 = null;
    view2131230905.setOnClickListener(null);
    view2131230905 = null;
    view2131230945.setOnClickListener(null);
    view2131230945 = null;
    view2131230892.setOnClickListener(null);
    view2131230892 = null;
    view2131231019.setOnClickListener(null);
    view2131231019 = null;
  }
}
