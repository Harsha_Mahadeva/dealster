// Generated code from Butter Knife. Do not modify!
package com.dealster.merchant.Fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.dealster.merchant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PreviewSurvey_ViewBinding implements Unbinder {
  private PreviewSurvey target;

  @UiThread
  public PreviewSurvey_ViewBinding(PreviewSurvey target, View source) {
    this.target = target;

    target.viewPager = Utils.findRequiredViewAsType(source, R.id.survey_pager, "field 'viewPager'", ViewPager.class);
    target.saveSubmitLayout = Utils.findRequiredViewAsType(source, R.id.save_submit, "field 'saveSubmitLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PreviewSurvey target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewPager = null;
    target.saveSubmitLayout = null;
  }
}
