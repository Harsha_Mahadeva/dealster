// Generated code from Butter Knife. Do not modify!
package com.dealster.merchant.Fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.dealster.merchant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Survey_Text_ViewBinding implements Unbinder {
  private Survey_Text target;

  @UiThread
  public Survey_Text_ViewBinding(Survey_Text target, View source) {
    this.target = target;

    target.survey_name = Utils.findRequiredViewAsType(source, R.id.survey_name, "field 'survey_name'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Survey_Text target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.survey_name = null;
  }
}
