// Generated code from Butter Knife. Do not modify!
package com.dealster.merchant.Fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.dealster.merchant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SurveyFragment_ViewBinding implements Unbinder {
  private SurveyFragment target;

  private View view2131230764;

  @UiThread
  public SurveyFragment_ViewBinding(final SurveyFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.promotional_recycler, "field 'recyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.add_promotion, "method 'addSurvey'");
    view2131230764 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.addSurvey();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SurveyFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;

    view2131230764.setOnClickListener(null);
    view2131230764 = null;
  }
}
