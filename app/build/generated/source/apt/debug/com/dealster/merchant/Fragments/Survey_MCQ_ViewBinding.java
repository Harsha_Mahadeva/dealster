// Generated code from Butter Knife. Do not modify!
package com.dealster.merchant.Fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.dealster.merchant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Survey_MCQ_ViewBinding implements Unbinder {
  private Survey_MCQ target;

  @UiThread
  public Survey_MCQ_ViewBinding(Survey_MCQ target, View source) {
    this.target = target;

    target.question = Utils.findRequiredViewAsType(source, R.id.question, "field 'question'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Survey_MCQ target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.question = null;
  }
}
