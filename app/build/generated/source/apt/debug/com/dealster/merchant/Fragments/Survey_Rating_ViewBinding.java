// Generated code from Butter Knife. Do not modify!
package com.dealster.merchant.Fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.dealster.merchant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Survey_Rating_ViewBinding implements Unbinder {
  private Survey_Rating target;

  @UiThread
  public Survey_Rating_ViewBinding(Survey_Rating target, View source) {
    this.target = target;

    target.ratingBar = Utils.findRequiredViewAsType(source, R.id.ratingBar, "field 'ratingBar'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Survey_Rating target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ratingBar = null;
  }
}
