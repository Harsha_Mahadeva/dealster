package com.dealster.merchant.Dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dealster.merchant.Activities.NewPromotion;
import com.dealster.merchant.Adapters.SimpleAdapter;
import com.dealster.merchant.Interfaces.OnItemClick;
import com.dealster.merchant.Network.NetworkUtility;
import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.AppController;
import com.dealster.merchant.Utilities.SP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by m on 2/26/18.
 */

public class TermsTemplate extends DialogFragment implements OnItemClick {

    RecyclerView list;
    AppCompatActivity appCompatActivity;
    SP sp;
    SparseArray<String> templateText = new SparseArray<>();
    Object selectedObject = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_terms_template, container, false);
        list = (RecyclerView) v.findViewById(R.id.template_list);
        Log.e("ASASASAS", "Reached");
        fetchTemplate();
        return v;
    }


    private void fetchTemplate() {

        sp = new SP(appCompatActivity);
        String URL = NetworkUtility.LIST_TEMPLATE;
        Log.e("URL", URL);
        Log.e("Access", sp.getAccessToken());
        JSONObject jsonObject = new JSONObject();
        templateText.clear();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("response", String.valueOf(response));
                try {
                    if (response.getString("status").equals("200")) {
                        JSONArray jsonArray = response.getJSONArray("templates");
                        int length = jsonArray.length();
                        for (int i = 0; i < length; i++) {
                            JSONObject eachObject = jsonArray.getJSONObject(i);
                            templateText.append(eachObject.getInt("id"), eachObject.getString("description"));
                        }
                        list.setAdapter(new SimpleAdapter(templateText, TermsTemplate.this));
                    } else {


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (AppCompatActivity) context;
    }

    @Override
    public void onClick(Object object) {
        Bundle bundle = (Bundle) object;
        ((NewPromotion) appCompatActivity).updateTerms(bundle.getInt("key"), bundle.getString("value"));
        this.dismiss();
    }


}
