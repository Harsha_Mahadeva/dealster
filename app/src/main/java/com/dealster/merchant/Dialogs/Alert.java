package com.dealster.merchant.Dialogs;

import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dealster.merchant.R;

public class Alert extends AlertDialog.Builder implements View.OnClickListener {

    private AlertDialog alertDialog;

    public Alert(@NonNull AppCompatActivity context, String message, String dismiss, int imageID) {
        super(context);
        setView(context, message, dismiss, imageID);
    }


    private void setView(AppCompatActivity context, String alertMessage, String dismiss, int imageID) {
        LayoutInflater inflater = context.getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        ImageView imageView = alertLayout.findViewById(R.id.image);
        imageView.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), imageID));
        TextView dismissLayout = alertLayout.findViewById(R.id.dismiss);
        dismissLayout.setText(dismiss);
        dismissLayout.setOnClickListener(this);
        ((TextView) alertLayout.findViewById(R.id.alert_message)).setText(alertMessage);
        this.setView(alertLayout);
        alertDialog = this.create();
    }

    @Override
    public void onClick(View v) {
        alertDialog.cancel();
    }


    public AlertDialog getAlertDialog() {
        return alertDialog;
    }


}
