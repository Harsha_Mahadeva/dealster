package com.dealster.merchant.Interfaces;

/**
 * Created by Harsha on 10/07/17.
 */

public interface OnRecyclerItemClick {

    void onItemClick(Object object, int position);
}
