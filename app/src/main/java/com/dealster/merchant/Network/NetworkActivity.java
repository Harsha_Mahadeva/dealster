package com.dealster.merchant.Network;


import io.reactivex.Observable;
import retrofit2.http.GET;

public interface NetworkActivity {

    @GET("authentication/getAppVersion/1")
    Observable<String> versionCheck();

    @GET("manage_branch/getBranchList")
    Observable<String> getBranchList();

    @GET("manage_branch/{branch_id}")
    Observable<String> getbranchDetails();
}
