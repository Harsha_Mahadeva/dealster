package com.dealster.merchant.Network;

/**
 * Created by Harsha on 21/06/17.
 */

public class NetworkUtility {
    public static final String BaseURL = "http://128.199.138.161:4001/api/v1/";
    public static final String LOGIN = BaseURL + "auth/sign_in";
    public static final String CHANGE_PASSWORD = BaseURL + "auth/change_password";
    public static final String MERCHANT_DETAILS = BaseURL + "merchant_details";
    public static final String FLAG_MERCHANT_DETAILS = BaseURL + "flag_np";
    public static final String FLAG_OWNER_DETAILS = BaseURL + "flag_acown";
    public static final String FLAG_ACCOUNT_MANAGER = BaseURL + "flag_mgr";
    public static final String REQUEST_OTP = BaseURL + "request_otp";
    public static final String VERIFY_OTP = BaseURL + "verify_otp";
    public static final String PROMO_LIST = BaseURL + "list_promotion";
    public static final String LIST_AGE = BaseURL + "list_age_template";
    public static final String LIST_LOCALITY = BaseURL + "fetch_locality";
    public static final String LIST_TEMPLATE = BaseURL + "list_template";
    public static final String UPDATE_PROMOTION = BaseURL + "update_promotion";
    public static final String SAVE_PROMOTION = BaseURL + "save_promotion";
    public static final String SAVE_TEMPLATE = BaseURL + "save_template";
}
