package com.dealster.merchant.Network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.dealster.merchant.Interfaces.Internet;


/**
 * Created by Harsha on 01/07/17.
 */

public class InternetConnectivity extends BroadcastReceiver {
    public static boolean is_connected = false;
    Internet internet;


    public InternetConnectivity(Internet internet) {
        this.internet = internet;
    }

    public static boolean is_connected() {
        return is_connected;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
            if (ni != null && ni.getState() == NetworkInfo.State.CONNECTED) {
                is_connected = true;
//                internet.ConnectionChanged(true);
            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                is_connected = false;
//                internet.ConnectionChanged(false);
            }
        }

    }

}
