package com.dealster.merchant.Network;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitHelper {


    private String authKey;

    public NetworkActivity getNetworkActivity(boolean plain, String authKey) {

        this.authKey = (authKey == null) ? "" : authKey;
        final Retrofit retrofit = (plain) ? createRetrofitPlain() : createRetrofit();
        return retrofit.create(NetworkActivity.class);
    }


    private Retrofit createRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(NetworkUtility.BaseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // <- add this
                .client(createOkHttpClient())
                .build();
    }

    private Retrofit createRetrofitPlain() {
        return new Retrofit.Builder()
                .baseUrl(NetworkUtility.BaseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // <- add this
                .client(createOkHttpClient())
                .build();
    }


    private OkHttpClient createOkHttpClient() {
        final OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request original = chain.request();
                final HttpUrl originalHttpUrl = original.url();

                final HttpUrl url = originalHttpUrl.newBuilder()
//                        .addQueryParameter("username", "demo")
                        .build();

                // Request customization: add request headers
//                final Request.Builder requestBuilder = original.newBuilder()
//                        .url(url);

                final Request.Builder requestBuilder = original.newBuilder().addHeader("authkey", authKey).addHeader("Content-Type", "application/json")
                        .url(url);

                final Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        return httpClient.build();
    }

}
