package com.dealster.merchant.Network;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dealster.merchant.R;


/**
 * Created by Harsha on 13/07/17.
 */

public class NoInternetView implements View.OnClickListener {
    private RelativeLayout no_internet;
    private AppCompatActivity appCompatActivity;


    public NoInternetView(AppCompatActivity appCompatActivity) {
//        no_internet = (RelativeLayout) appCompatActivity.findViewById(R.id.no_internet);
//        TextView call_to_order = (TextView) appCompatActivity.findViewById(R.id.call_order);
//        call_to_order.setOnClickListener(this);
//        this.appCompatActivity = appCompatActivity;
//        if (!isNetworkAvailable())
//            no_internet.setVisibility(View.VISIBLE);
//        else
//            no_internet.setVisibility(View.GONE);
    }

    public void update(boolean isconnected) {
        if (isconnected)
            no_internet.setVisibility(View.GONE);
        else
            no_internet.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        call();
    }

    private void call() {
        if (ActivityCompat.checkSelfPermission(appCompatActivity, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
//            Intent intent = new Intent(Intent.ACTION_CALL);
//            intent.setData(Uri.parse("tel:" + appCompatActivity.getResources().getString(R.string.text_order_number)));
//            appCompatActivity.startActivity(intent);
        } else {
            permission();
        }
    }

    private void permission() {

//        if (ActivityCompat.shouldShowRequestPermissionRationale(appCompatActivity,
//                Manifest.permission.CALL_PHONE)) {
//            ActivityCompat.requestPermissions(appCompatActivity,
//                    new String[]{Manifest.permission.CALL_PHONE},
//                    Constants.NO_INTERNET);
//        } else {
//            ActivityCompat.requestPermissions(appCompatActivity,
//                    new String[]{Manifest.permission.CALL_PHONE},
//                    Constants.NO_INTERNET);
//        }
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        if (requestCode == Constants.NO_INTERNET) {
//            if (grantResults.length > 0
//                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                call();
//            } else {
//                new AlertDialog.Builder(new ContextThemeWrapper(appCompatActivity, android.R.style.Theme_Holo_Light_Dialog_NoActionBar))
//                        .setTitle(appCompatActivity.getResources().getString(R.string.text_call_permission))
//                        .setMessage(appCompatActivity.getResources().getString(R.string.text_permission_denied))
//                        .setPositiveButton(appCompatActivity.getResources().getString(R.string.text_allow), new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                permission();
//                            }
//                        })
//                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                appCompatActivity.finish();
//                            }
//                        })
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .show();
//            }
//        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) appCompatActivity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
