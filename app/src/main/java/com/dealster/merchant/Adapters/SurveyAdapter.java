package com.dealster.merchant.Adapters;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dealster.merchant.Interfaces.OnItemClick;
import com.dealster.merchant.Interfaces.PageChange;
import com.dealster.merchant.Models.PromoList;
import com.dealster.merchant.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.ViewHolder> {
    AppCompatActivity appCompatActivity;
    ArrayList<PromoList> arrayList;
    private final int BODY = 1;
    private final int FOOTER = 2;
    public boolean pageEnd = false;
    PageChange pageChange;
    OnItemClick onItemClick;


    public SurveyAdapter(ArrayList<PromoList> arrayList, AppCompatActivity appCompatActivity, PageChange pageChange, OnItemClick onItemClick) {
        this.appCompatActivity = appCompatActivity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClick;
        this.pageChange = pageChange;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        if (viewType == BODY)
            view = inflater.inflate(R.layout.promotion_view, parent, false);
        else
            view = inflater.inflate(R.layout.page_end, parent, false);
        return new ViewHolder(view, viewType);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == getItemCount() - 1) {
            if (pageEnd) {
                holder.progressBar.setVisibility(View.GONE);
                holder.page_end.setVisibility(View.VISIBLE);
            } else {
                pageChange.nextPage();
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.page_end.setVisibility(View.GONE);
            }
            return;
        }
        holder.promo_title.setTextColor(appCompatActivity.getResources().getColor(android.R.color.black));
        PromoList promoList = arrayList.get(position);
        holder.promo_title.setText(promoList.getPromo_title());
        holder.start_date.setText(promoList.getFrom_date());
        holder.end_date.setText(promoList.getTo_date());

        switch (promoList.getPromo_status()) {
            case 1:
                holder.status.setText("Draft");
                holder.status.setTextColor(appCompatActivity.getResources().getColor(R.color.status_draft));
                holder.edit.setVisibility(View.VISIBLE);
                break;
            case 2:
                holder.edit.setVisibility(View.GONE);
                holder.status.setText("Planned");
                holder.status.setTextColor(appCompatActivity.getResources().getColor(R.color.status_planned));

                break;
            case 3:
                holder.edit.setVisibility(View.GONE);
                holder.status.setText("Active");
                holder.status.setTextColor(appCompatActivity.getResources().getColor(android.R.color.black));

                break;
            case 4:
                holder.edit.setVisibility(View.GONE);
                holder.status.setText("Archive");
                holder.promo_title.setTextColor(appCompatActivity.getResources().getColor(R.color.status_draft));
                holder.status.setTextColor(appCompatActivity.getResources().getColor(R.color.status_archived));
                break;
            default:
                holder.edit.setVisibility(View.VISIBLE);
                holder.status.setText("Draft");
                holder.status.setTextColor(appCompatActivity.getResources().getColor(R.color.status_draft));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == getItemCount() - 1) ? FOOTER : BODY;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView promo_title, start_date, end_date, status, page_end;
        Button edit;
        ProgressBar progressBar;
        CardView parent;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            if (viewType == BODY) {
                promo_title = itemView.findViewById(R.id.promotion_name);
                start_date = itemView.findViewById(R.id.start_date);
                end_date = itemView.findViewById(R.id.end_date);
                status = itemView.findViewById(R.id.promotion_status);
                edit = itemView.findViewById(R.id.edit_promo);
                parent = itemView.findViewById(R.id.parent);
                parent.setOnClickListener(this);
                edit.setOnClickListener(this);
            } else {
                progressBar = itemView.findViewById(R.id.progress_bar);
                page_end = itemView.findViewById(R.id.page_end);
                if (pageEnd) {
                    progressBar.setVisibility(View.GONE);
                    page_end.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    page_end.setVisibility(View.GONE);
                }
            }

        }

        @Override
        public void onClick(View v) {

            int view_type = v.getId() == R.id.parent ? 0 : 1;

            try {
                JSONObject jsonObject = new JSONObject(arrayList.get(getLayoutPosition()).getJson());
                jsonObject.put("view_type", view_type);
                onItemClick.onClick(jsonObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
}
