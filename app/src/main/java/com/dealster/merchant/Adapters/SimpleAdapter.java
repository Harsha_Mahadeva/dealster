package com.dealster.merchant.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dealster.merchant.Interfaces.OnItemClick;
import com.dealster.merchant.R;

/**
 * Created by m on 2/26/18.
 */

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.ViewHolder> {

    private SparseArray<String> array;
    private OnItemClick onItemClick;
    private Context context;

    public SimpleAdapter(SparseArray<String> array, OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
        this.array = array;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        context = parent.getContext();
        View v = inflater.inflate(R.layout.simple_text, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(array.valueAt(position));
        if (position % 2 == 0)
            holder.textView.setBackgroundColor(context.getResources().getColor(R.color.light_grey));
        else
            holder.textView.setBackgroundColor(context.getResources().getColor(android.R.color.white));

    }

    @Override
    public int getItemCount() {
        return array.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.simple_text);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("key", array.keyAt(getLayoutPosition()));
                    bundle.putString("value", array.valueAt(getLayoutPosition()));
                    onItemClick.onClick(bundle);
                }
            });
        }
    }
}

