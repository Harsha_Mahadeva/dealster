package com.dealster.merchant.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dealster.merchant.Models.NameID;
import com.dealster.merchant.R;

import java.util.ArrayList;
import java.util.List;

public class NameIDAdapter extends ArrayAdapter<NameID> implements Filterable {

    public List<NameID> localityDataArrayList;
    private List<NameID> dataListAllItems;
    private ListFilter listFilter = new ListFilter();


    public NameIDAdapter(@NonNull Context context, int resource, List<NameID> localityDataArrayList) {
        super(context, resource);
        this.localityDataArrayList = localityDataArrayList;
        dataListAllItems = localityDataArrayList;
    }

    @Override
    public int getCount() {
        return localityDataArrayList.size();
    }

    @Nullable
    @Override
    public NameID getItem(int position) {
        return localityDataArrayList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {

            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.simple_list_text, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        NameID localityData = getItem(position);
        viewHolder.name.setText(localityData.getName());
        viewHolder.name.setBackgroundColor(parent.getContext().getResources().getColor((position % 2 == 1) ? R.color.light_grey : android.R.color.white));
        return convertView;


    }


    class ViewHolder {
        TextView name;

        public ViewHolder(View v) {
            name = v.findViewById(R.id.simple_text);
        }


    }

    @NonNull
    @Override
    public Filter getFilter() {
        return listFilter;
    }

    public class ListFilter extends Filter {
        private Object lock = new Object();

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();
            if (dataListAllItems == null) {
                synchronized (lock) {
                    dataListAllItems = new ArrayList<>(localityDataArrayList);
                }
            }

            if (prefix == null || prefix.length() == 0) {
                synchronized (lock) {
                    results.values = dataListAllItems;
                    results.count = dataListAllItems.size();
                }
            } else {
                final String searchStrLowerCase = prefix.toString().toLowerCase();

                ArrayList<NameID> matchValues = new ArrayList<>();

                for (NameID dataItem : dataListAllItems) {
                    if (dataItem.getName().toLowerCase().startsWith(searchStrLowerCase)) {
                        matchValues.add(dataItem);
                    }
                }

                results.values = matchValues;
                results.count = matchValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                localityDataArrayList = (ArrayList<NameID>) results.values;
            } else {
                localityDataArrayList = null;
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

    }
}
