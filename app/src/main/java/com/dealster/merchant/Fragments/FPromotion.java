package com.dealster.merchant.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dealster.merchant.Activities.Main;
import com.dealster.merchant.Activities.NewPromotion;
import com.dealster.merchant.Adapters.PromotionAdapter;
import com.dealster.merchant.Interfaces.OnItemClick;
import com.dealster.merchant.Interfaces.PageChange;
import com.dealster.merchant.Models.PromoList;
import com.dealster.merchant.Network.NetworkUtility;
import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.AppController;
import com.dealster.merchant.Utilities.ProgressDialogueUtility;
import com.dealster.merchant.Utilities.SP;
import com.github.clans.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by m on 2/22/18.
 */

public class FPromotion extends Fragment implements View.OnClickListener, PageChange, OnItemClick {

    AppCompatActivity appCompatActivity;
    RecyclerView recyclerView;
    ProgressDialogueUtility progressDialogueUtility;
    PromotionAdapter promotionAdapter;
    SP sp;
    ArrayList<PromoList> arrayList;
    private final int codeNewPromo = 1;
    FloatingActionButton floatingActionButton;
    private int nextPage = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((Main) appCompatActivity).setTitle("Promotion");
        View view = inflater.inflate(R.layout.promotions, container, false);
        recyclerView = view.findViewById(R.id.promotional_recycler);
        progressDialogueUtility = new ProgressDialogueUtility(appCompatActivity);
        floatingActionButton = view.findViewById(R.id.add_promotion);
        floatingActionButton.setColorNormal(appCompatActivity.getResources().getColor(R.color.green_color_dark));
        floatingActionButton.setColorPressed(appCompatActivity.getResources().getColor(R.color.green_color_dark));
        floatingActionButton.setOnClickListener(this);
        sp = new SP(appCompatActivity);
        arrayList = new ArrayList<>();
        promotionAdapter = new PromotionAdapter(arrayList, appCompatActivity, this, this);
        recyclerView.setAdapter(promotionAdapter);
        getPromotions(0);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.appCompatActivity = (AppCompatActivity) context;
    }


    private void getPromotions(final int page) {
        Log.e("Page No", String.valueOf(page));
        if (page == 0) {
            arrayList.clear();
            progressDialogueUtility.StartProgressNonCancelable();
        }

        String URL = NetworkUtility.PROMO_LIST;
        Log.e("URL", URL);
        Log.e("Access", sp.getAccessToken());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("offset", page);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (page == 0)
                    progressDialogueUtility.progressDialog.cancel();

                Log.e("response", String.valueOf(response));
                try {
                    if (response.getString("status").equals("200")) {


                        JSONArray jsonArray = response.getJSONArray("results");
                        int length = jsonArray.length();
                        if (length == 0) {
                            promotionAdapter.pageEnd = true;
                            if (nextPage > 0)
                                nextPage--;
                        }
                        for (int i = 0; i < length; i++) {
                            JSONObject eachObject = jsonArray.getJSONObject(i);
                            PromoList promoList = new PromoList();

                            promoList.setAge_group(eachObject.getString("age_group"));
                            promoList.setBookingType(eachObject.getInt("booking_type"));
                            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            try {

                                promoList.setDate_time(outputFormat.format(inputFormat.parse(eachObject.getString("date_time"))));
                                promoList.setFrom_date(outputFormat.format(inputFormat.parse(eachObject.getString("from_date"))));
                                promoList.setTo_date(outputFormat.format(inputFormat.parse(eachObject.getString("to_date"))));
                            } catch (ParseException e) {
                                Snackbar snackbar = Snackbar.make(recyclerView, "Invalid Date found", Snackbar.LENGTH_SHORT);
                                snackbar.getView().setBackgroundColor(appCompatActivity.getResources().getColor(android.R.color.white));
                                snackbar.show();
                                e.printStackTrace();
                            }
                            promoList.setPromo_id(eachObject.getInt("id"));
                            promoList.setUser_id(eachObject.getInt("user_id"));
                            promoList.setMaxNoOfCoupons(eachObject.getInt("max_number_of_coupons"));
                            promoList.setTemplate_id(eachObject.getInt("template_id"));
                            promoList.setPromo_title(eachObject.getString("title"));
                            promoList.setPromo_desc(eachObject.getString("detailed_description"));
                            promoList.setNotification_message(eachObject.getString("notification_message"));
                            promoList.setLocality(eachObject.getString("locality"));
                            promoList.setPromo_status(eachObject.getInt("status"));
                            promoList.setJson(eachObject.toString());
                            arrayList.add(promoList);
                        }
                        promotionAdapter.notifyDataSetChanged();
                    } else {
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                    Snackbar snackbar = Snackbar.make(recyclerView, "Invalid JSON Format", Snackbar.LENGTH_SHORT);
                    snackbar.getView().setBackgroundColor(appCompatActivity.getResources().getColor(android.R.color.white));
                    snackbar.show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
                if (page == 0)
                    progressDialogueUtility.progressDialog.cancel();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onClick(View v) {
        startActivityForResult(new Intent(appCompatActivity, NewPromotion.class), codeNewPromo);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Triggered", "Yes");
        if (requestCode == codeNewPromo) {
            if (resultCode == Activity.RESULT_OK) {
                promotionAdapter.pageEnd = false;
                arrayList.clear();
                nextPage = 0;
                getPromotions(0);
            }
        }
    }

    @Override
    public void nextPage() {
        nextPage++;
        getPromotions(nextPage);
    }

    @Override
    public void onClick(Object object) {
        String jsonObject = (String) object;
        Intent intent = new Intent(appCompatActivity, NewPromotion.class);


        intent.putExtra("data", jsonObject);
        startActivityForResult(intent, codeNewPromo);
    }
}
