package com.dealster.merchant.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dealster.merchant.R;

/**
 * Created by Harsha on 14/10/16.
 */
public class Accepted extends Fragment {

    RecyclerView recyclerview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pending, container, false);
        recyclerview = (RecyclerView) v.findViewById(R.id.recycler1);
        return v;
    }
}
