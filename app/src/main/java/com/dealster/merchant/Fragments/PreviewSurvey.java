package com.dealster.merchant.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.dealster.merchant.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreviewSurvey extends BaseFragment implements ViewPager.OnPageChangeListener {

    @BindView(R.id.survey_pager)
    ViewPager viewPager;

    @BindView(R.id.save_submit)
    LinearLayout saveSubmitLayout;

    SparseArray<String> fragmentMap = new SparseArray<>();

    MyPager myPager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.survey_preview, container, false);
        unbinder = ButterKnife.bind(this, view);
        myPager = new MyPager(appCompatActivity.getSupportFragmentManager());
        viewPager.setAdapter(myPager);
        viewPager.addOnPageChangeListener(this);
        return view;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == Objects.requireNonNull(viewPager.getAdapter()).getCount() - 1)
            saveSubmitLayout.setVisibility(View.VISIBLE);
        else
            saveSubmitLayout.setVisibility(View.GONE);

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    class MyPager extends FragmentPagerAdapter {

        public MyPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;


            int who = fragmentMap.keyAt(position);
            String value = fragmentMap.valueAt(position);
            switch (who) {
                case 2:
                    fragment = Survey_Text.newInstance(value, position + 1);
                    break;
                case 3:
                    fragment = Survey_Rating.newInstance(value, position + 1);
                    break;
                case 1:
                    fragment = Survey_MCQ.newInstance(value, position + 1);
                    break;

            }


            return fragment;
        }

        @Override
        public int getCount() {
            return fragmentMap.size();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEvent(JSONObject jsonObject) {

        Log.e("EVENT", jsonObject.toString());
        try {
            int who = jsonObject.getInt("who");
            String value;
            if (who == 1)
                value = jsonObject.getJSONObject("value").toString();
            else
                value = jsonObject.getString("value");
            if (fragmentMap.get(who, null) == null && !TextUtils.isEmpty(value)) {
                fragmentMap.delete(who);
                fragmentMap.put(who, value);
            } else if (fragmentMap.get(who, null) != null) {
                if (TextUtils.isEmpty(value))
                    fragmentMap.delete(who);
                else
                    fragmentMap.put(who, value);
            }
            myPager.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}
