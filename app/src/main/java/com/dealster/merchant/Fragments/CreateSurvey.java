package com.dealster.merchant.Fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.dealster.merchant.Activities.ActivitySurvey;
import com.dealster.merchant.R;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewAfterTextChangeEvent;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTouch;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CreateSurvey extends BaseFragment implements Consumer<TextViewAfterTextChangeEvent> {

    @BindView(R.id.from_date)
    Button fromButton;

    @BindView(R.id.option_layout)
    LinearLayout optionLayout;

    @BindView(R.id.rating_question_layout)
    LinearLayout ratingLayout;

    @BindView(R.id.text_question_layout)
    LinearLayout textLayout;

    @BindView(R.id.terms)
    EditText terms;

    @BindView(R.id.text_question)
    EditText textQuestion;

    @BindView(R.id.text_block)
    EditText textBlock;

    @BindView(R.id.rating_question)
    EditText ratingQuestion;
    @BindView(R.id.option1)
    EditText option1;
    @BindView(R.id.option2)
    EditText option2;
    @BindView(R.id.option3)
    EditText option3;
    @BindView(R.id.option4)
    EditText option4;
    @BindView(R.id.option5)
    EditText option5;


    int currentCount = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_survey, container, false);
        unbinder = ButterKnife.bind(this, view);
        setRootView(view);
        registerRx();
        return view;
    }


    @OnClick({R.id.from_date, R.id.to_date})
    public void datePicker(final Button view) {

        final Calendar c = Calendar.getInstance();
        final Calendar from = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        long diff = 0;


        if (view.getId() == R.id.to_date) {
            if (!fromButton.getText().toString().contains("Date")) {
                try {
                    Date date1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(fromButton.getText().toString());
                    from.setTime(date1);
                    diff = from.getTimeInMillis() - c.getTimeInMillis();
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            } else {
                SnackBarMessage("Please choose start date");
                return;
            }

        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(appCompatActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int monthOfYear, int dayOfMonth) {
                        String date_time = year + "-" + new DecimalFormat("00").format(monthOfYear + 1) + "-" + new DecimalFormat("00").format(dayOfMonth);
                        view.setText(date_time);


                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis() + diff);// set today's date as min date
        datePickerDialog.show();

    }

    @OnClick(R.id.next)
    public void next() {
        ((ActivitySurvey) appCompatActivity).openPreview();
    }


    @OnClick({R.id.rating, R.id.multiple_choice, R.id.text})
    public void addScreen(View view) {

        int id = view.getId();

        switch (id) {
            case R.id.rating:
                ratingLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.multiple_choice:
                optionLayout.setVisibility(View.VISIBLE);
                setUpChoiceView(1);
                break;
            case R.id.text:
                textLayout.setVisibility(View.VISIBLE);
                break;
        }


    }


    public void setUpChoiceView(int count) {
        currentCount = count;
        for (int i = 0; i < optionLayout.getChildCount(); i++) {
            if (i == 0)
                continue;
            if (i == count) {
                optionLayout.getChildAt(i).setVisibility(View.VISIBLE);
                optionLayout.getChildAt(i).setAlpha(.3f);
            } else if (i < count) {
                optionLayout.getChildAt(i).setVisibility(View.VISIBLE);
                optionLayout.getChildAt(i).setAlpha(1f);
            } else
                optionLayout.getChildAt(i).setVisibility(View.GONE);
        }
    }


    @OnTouch({R.id.option1, R.id.option2, R.id.option3, R.id.option4, R.id.option5})
    public boolean onTouch(View view) {
        int id = view.getId();
        int tempPosition = 0;
        switch (id) {
            case R.id.option1:
                tempPosition = 1;
                break;
            case R.id.option2:
                tempPosition = 2;
                break;
            case R.id.option3:
                tempPosition = 3;
                break;
            case R.id.option4:
                tempPosition = 4;
                break;
            case R.id.option5:
                tempPosition = 5;
                break;
        }

        if (tempPosition > 1) {
            EditText editText = (EditText) optionLayout.getChildAt(tempPosition - 1);
            if (editText.getText().toString().isEmpty()) {
                SnackBarMessage("Please fill the empty options to add new");
                return true;
            }
        }


        Log.e("Touch", String.valueOf(id));
        if (currentCount == tempPosition)
            setUpChoiceView(currentCount + 1);
        return false;
    }


    @OnTouch({R.id.text_block, R.id.text_question, R.id.rating_question})
    public boolean onClear(View view, MotionEvent event) {
        final int DRAWABLE_RIGHT = 2;
        EditText textView = (EditText) view;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (event.getRawX() >= (textView.getRight() - textView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                textView.setText("");
                hideKeyboard();
                if (textView.getId() == R.id.text_question) {
                    ((View) textView.getParent().getParent()).setVisibility(View.GONE);
                    currentCount = 0;
                } else
                    ((View) textView.getParent()).setVisibility(View.GONE);
            }
        }

        return false;
    }


    private void registerRx() {

        mCompositeDisposable.add(RxTextView.afterTextChangeEvents(textBlock).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(this));
        mCompositeDisposable.add(RxTextView.afterTextChangeEvents(ratingQuestion).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(this));
        mCompositeDisposable.add(RxTextView.afterTextChangeEvents(textQuestion).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(this));
        mCompositeDisposable.add(RxTextView.afterTextChangeEvents(option1).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(this));
        mCompositeDisposable.add(RxTextView.afterTextChangeEvents(option2).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(this));
        mCompositeDisposable.add(RxTextView.afterTextChangeEvents(option3).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(this));
        mCompositeDisposable.add(RxTextView.afterTextChangeEvents(option4).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(this));
        mCompositeDisposable.add(RxTextView.afterTextChangeEvents(option5).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(this));
    }


    @Override
    public void accept(TextViewAfterTextChangeEvent textViewAfterTextChangeEvent) throws Exception {
        EditText editText = (EditText) textViewAfterTextChangeEvent.view();
        JSONObject jsonObject = new JSONObject();
        int who = 0;
        switch (editText.getId()) {

            case R.id.text_question:
                who = 1;
                break;
            case R.id.text_block:
                who = 2;
                break;
            case R.id.rating_question:
                who = 3;
                break;
            default:
                who = 1;
                break;
        }
        jsonObject.put("who", who);

        if (who != 1) {
            jsonObject.put("value", editText.getText().toString().trim());
        } else {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("text", textQuestion.getText().toString());
            JSONArray jsonArray = new JSONArray();
            for (int i = 1; i < 6; i++) {
                EditText editText1 = (EditText) optionLayout.getChildAt(i);
                if (!editText1.getText().toString().isEmpty())
                    jsonArray.put(editText1.getText().toString());
            }
            jsonObject1.put("list", jsonArray);
            if (textQuestion.getText().toString().isEmpty() || jsonArray.length() <= 0)
                jsonObject.put("value", "");
            else {
                jsonObject.put("value", jsonObject1);
            }

        }
        EventBus.getDefault().post(jsonObject);

    }


}
