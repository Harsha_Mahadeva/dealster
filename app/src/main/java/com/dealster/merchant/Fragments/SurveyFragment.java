package com.dealster.merchant.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dealster.merchant.Activities.ActivitySurvey;
import com.dealster.merchant.R;
import com.github.clans.fab.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SurveyFragment extends BaseFragment {
    @BindView(R.id.promotional_recycler)
    RecyclerView recyclerView;
    int current_page = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.promotions, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @OnClick(R.id.add_promotion)
    public void addSurvey() {
        startActivity(new Intent(appCompatActivity, ActivitySurvey.class));
    }


    public void loadSurvey() {


    }
}
