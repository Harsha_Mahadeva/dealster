package com.dealster.merchant.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dealster.merchant.R;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Survey_Text extends BaseFragment {


    public static Survey_Text newInstance(String text, int pageNumber) {
        Survey_Text f = new Survey_Text();
        Bundle bdl = new Bundle();
        bdl.putString("text", text);
        bdl.putInt("page", pageNumber);
        f.setArguments(bdl);
        return f;
    }


    @BindView(R.id.survey_name)
    TextView survey_name;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.text_question, container, false);
        unbinder = ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        assert bundle != null;
        survey_name.setText(String.format(Locale.ENGLISH, "%s. %s", bundle.getInt("page"), bundle.getString("text")));
        return view;
    }

}
