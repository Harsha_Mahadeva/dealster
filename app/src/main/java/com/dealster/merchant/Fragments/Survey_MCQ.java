package com.dealster.merchant.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dealster.merchant.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Survey_MCQ extends BaseFragment {


    public static Survey_MCQ newInstance(String text, int pageNumber) {
        Survey_MCQ f = new Survey_MCQ();
        Bundle bdl = new Bundle();
        bdl.putString("text", text);
        bdl.putInt("page", pageNumber);
        f.setArguments(bdl);
        return f;
    }

    @BindView(R.id.question)
    TextView question;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.multiple_choice, container, false);
        Bundle bundle = getArguments();
        unbinder = ButterKnife.bind(this, view);

        try {
            JSONObject jsonObject = new JSONObject(bundle.getString("text"));
            Log.e("Bundle", jsonObject.toString());

            question.setText(String.format(Locale.ENGLISH, "%s. %s", bundle.getInt("page"), jsonObject.getString("text")));
            JSONArray jsonArray = jsonObject.getJSONArray("list");
            LinearLayout linearLayout = (LinearLayout) view;

            int len = jsonArray.length();
            for (int i = 0; i < len; i++) {
                LinearLayout view1 = (LinearLayout) inflater.inflate(R.layout.mcq_option, linearLayout, false);
                TextView textView = view1.findViewById(R.id.indicator);
                TextView option = view1.findViewById(R.id.option);
                StringBuffer buffer = new StringBuffer();
                buffer.append((char) ('A' + i));
                textView.setText(buffer.toString());
                option.setText(jsonArray.getString(i));
                linearLayout.addView(view1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }
}
