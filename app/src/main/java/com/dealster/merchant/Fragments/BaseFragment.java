package com.dealster.merchant.Fragments;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.dealster.merchant.Utilities.ProgressDialogueUtility;
import com.dealster.merchant.Utilities.SP;

import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

public class BaseFragment extends Fragment {


    AppCompatActivity appCompatActivity;
    ProgressDialogueUtility progressDialogueUtility;
    SP sp;
    public CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    Unbinder unbinder;
    View root;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.appCompatActivity = (AppCompatActivity) context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    public void hideKeyboard() {
        View view = appCompatActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) appCompatActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void SnackBarMessage(String message) {
        if (root != null) {
            Snackbar snackbar = Snackbar.make(root, message, Snackbar.LENGTH_SHORT);
            snackbar.getView().setBackgroundColor(getResources().getColor(android.R.color.white));
            snackbar.show();
        }

    }

    public void setRootView(View parent) {
        this.root = parent;
    }


}
