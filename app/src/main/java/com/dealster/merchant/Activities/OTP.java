package com.dealster.merchant.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dealster.merchant.Dialogs.Alert;
import com.dealster.merchant.Network.NetworkUtility;
import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.AppController;
import com.dealster.merchant.Utilities.ProgressDialogueUtility;
import com.dealster.merchant.Utilities.SP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Harsha on 06/04/17.
 */

public class OTP extends AppCompatActivity implements View.OnClickListener {

    EditText otp;
    TextView resendOTP;
    Button continue_button;
    CoordinatorLayout coordinatorLayout;
    ProgressDialogueUtility progressDialogueUtility;
    SP sp;
    int tries = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        init();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }


    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.resend_otp) {
            tries++;
            if (tries > 2) {
                AlertDialog alertDialog = new Alert(OTP.this, "Dealster will be contacting you to resolve the OTP issue. Your account creation is suspended for now.", "OK", R.mipmap.alert_message).getAlertDialog();
                alertDialog.show();
                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        sp.clearData();
                        OTP.this.finish();
                        startActivity(new Intent(OTP.this, Login.class));
                    }
                });
            }


            requestOTP();
            return;
        }

        if (otp.getText().toString().isEmpty() || otp.getText().length() < 4) {
            SnackBarMessage("OTP cannot be empty");
            return;
        }
        progressDialogueUtility.StartProgressNonCancelable();

        String URL = NetworkUtility.VERIFY_OTP;
        JSONObject json = new JSONObject();
        try {
            json.put("otp", otp.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("response", String.valueOf(response));
                progressDialogueUtility.progressDialog.cancel();
                try {
                    if (response.getInt("status") == 200) {
                        SnackBarMessage("OTP verified");
                        startActivity(new Intent(OTP.this, Subscriptions.class));
//                        finish();
                        sp.updateOTPstatus(true);
                    } else {
                        AlertDialog alertDialog = new Alert(OTP.this, "The OTP entered is incorrect. Please try again.", "OK", R.mipmap.alert_message).getAlertDialog();
                        alertDialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, errorListener) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);


    }


    private void SnackBarMessage(String message) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(getResources().getColor(android.R.color.white));
        snackbar.show();
    }


    private void requestOTP() {

        progressDialogueUtility.StartProgressNonCancelable();

        String URL = NetworkUtility.REQUEST_OTP;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("response", String.valueOf(response));

                progressDialogueUtility.progressDialog.cancel();
                try {
                    if (response.getInt("status") == 200) {
                        SnackBarMessage("OTP is sent to your registered phone");
                    } else {
                        SnackBarMessage("Request for the otp failed");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, errorListener) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }


    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            progressDialogueUtility.progressDialog.cancel();
            NetworkResponse response = error.networkResponse;
            SnackBarMessage("Request for the otp failed");
            Log.e("Response Error", new String(response.data));
            if (response.statusCode == 401) {
                SnackBarMessage(OTP.this.getResources().getString(R.string.error_auth));
            }
        }
    };


    private void init() {
        sp = new SP(this);
        otp = (EditText) findViewById(R.id.otp);
        continue_button = (Button) findViewById(R.id.continue_button);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        continue_button.setOnClickListener(this);
        progressDialogueUtility = new ProgressDialogueUtility(this);
        resendOTP = (TextView) findViewById(R.id.resend_otp);
        resendOTP.setOnClickListener(this);
        requestOTP();

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
