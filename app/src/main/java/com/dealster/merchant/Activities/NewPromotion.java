package com.dealster.merchant.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.dealster.merchant.Adapters.NameIDAdapter;
import com.dealster.merchant.Dialogs.Alert;
import com.dealster.merchant.Models.NameID;
import com.dealster.merchant.Network.NetworkUtility;
import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.AppController;
import com.dealster.merchant.Utilities.SP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class NewPromotion extends AppCompatActivity implements View.OnClickListener, TextWatcher, TagView.OnTagDeleteListener {

    Toolbar toolbar;
    CoordinatorLayout parent;

    EditText promo_title, max_coupons, notification_message, desc, terms;
    Button from_date, from_time, to_date, to_time, save_template;
    Spinner audience;
    Switch booking_type;
    SP sp;
    TextView select_template;
    ArrayAdapter<String> arrayAdapter;
    ArrayList<String> target = new ArrayList<>();

    String localityString = "";
    String ageString = "";
    String currentTemplate = "";
    int termsID = 0;
    int targetAudience = 0;
    int promotionID = 0;
    Menu menu = null;
    final int ACT = 100;
    int currentStatus = 0;
    RelativeLayout progressBar;
    List<NameID> localityData = new ArrayList<>();
    List<NameID> ageGroupData = new ArrayList<>();
    AutoCompleteTextView autoCompleteTextView;
    AutoCompleteTextView ageGroupEdit;
    NameIDAdapter nameIDAdapter;
    NameIDAdapter ageGroupAdapter;
    TagView tagView;
    TagView ageGroup;
    Button saveButton, submitButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_promotion);
        toolbar = findViewById(R.id.toolbar);
        sp = new SP(this);
        parent = findViewById(R.id.coordinator_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        select_template = findViewById(R.id.select_template);
        select_template.setOnClickListener(this);
        ageGroupEdit = findViewById(R.id.age_group);
        ageGroup = findViewById(R.id.age_group_layout);
        promo_title = findViewById(R.id.promo_name);
        promo_title.setSelected(true);
        progressBar = findViewById(R.id.progress_bar);
        max_coupons = findViewById(R.id.max_coupons);
        notification_message = findViewById(R.id.notification_message);
        desc = findViewById(R.id.desc);
        terms = findViewById(R.id.terms);
        terms.addTextChangedListener(this);
        from_date = findViewById(R.id.from_date);
        save_template = findViewById(R.id.save_template);
        save_template.setOnClickListener(this);
        to_date = findViewById(R.id.to_date);
        from_time = findViewById(R.id.from_time);
        to_time = findViewById(R.id.to_time);
        audience = findViewById(R.id.target_audience);
        booking_type = findViewById(R.id.booking_type);
        autoCompleteTextView = findViewById(R.id.locality_);
        from_date.setOnClickListener(this);
        from_time.setOnClickListener(this);
        to_time.setOnClickListener(this);
        to_date.setOnClickListener(this);
        saveButton = findViewById(R.id.save);
        saveButton.setOnClickListener(this);
        submitButton = findViewById(R.id.submit);
        submitButton.setOnClickListener(this);
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, target);
        nameIDAdapter = new NameIDAdapter(this, R.layout.simple_text, localityData);
        ageGroupAdapter = new NameIDAdapter(this, R.layout.simple_text, ageGroupData);
        ageGroupEdit.setAdapter(ageGroupAdapter);

        autoCompleteTextView.setAdapter(nameIDAdapter);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NameID nameID = (NameID) parent.getItemAtPosition(position);
                autoCompleteTextView.setText("");

                Tag tag = new Tag(nameID.getName());
                tag.setId(nameID.getId());
                List<Tag> tags = tagView.getTags();
                Log.e("current Tags", tags.toString());

                if (tags.contains(tag)) {
                    SnackMessage("Locality Already Added");
                    return;
                }
                tagView.addTag(tag);
            }
        });
        ageGroupEdit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                NameID nameID = (NameID) parent.getItemAtPosition(position);

                ageGroupEdit.setText("");

                Tag tag = new Tag(nameID.getName());
                tag.setId(nameID.getId());
                List<Tag> tags = ageGroup.getTags();
                if (tags.contains(tag)) {
                    SnackMessage("Age group already added Already Added");
                    return;
                }
                ageGroup.addTag(tag);

            }
        });

        audience.setAdapter(arrayAdapter);
        tagView = findViewById(R.id.locality_layout_);
        tagView.setOnTagDeleteListener(this);
        ageGroup.setOnTagDeleteListener(this);

        Intent intent = getIntent();
        if (intent.getStringExtra("data") != null) {

            Log.e("Data", intent.getStringExtra("data"));

            try {
                JSONObject jsonObject = new JSONObject(intent.getStringExtra("data"));
                if (jsonObject.getInt("view_type") == 0) {
                    toggleFocus(false);
                    getSupportActionBar().setTitle("View Promotion");
                } else
                    getSupportActionBar().setTitle("Edit Promotion");

                promo_title.setText(jsonObject.getString("title"));
                promotionID = jsonObject.getInt("id");
                currentStatus = jsonObject.getInt("status");
                max_coupons.setText(jsonObject.getString("max_number_of_coupons"));
                notification_message.setText(jsonObject.getString("notification_message"));
                desc.setText(jsonObject.getString("detailed_description"));
                String from_date_ = "";
                String to_date_ = "";
                try {

                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    from_date_ = outputFormat.format(inputFormat.parse(jsonObject.getString("date_time")));
                    to_date_ = outputFormat.format(inputFormat.parse(jsonObject.getString("to_date")));
                    String[] from = from_date_.split(" ");
                    String[] to = to_date_.split(" ");
                    from_date.setText(from[0]);
                    from_time.setText(from[1]);
                    to_date.setText(to[0]);
                    to_time.setText(to[1]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                localityString = jsonObject.getString("locality");
                ageString = jsonObject.getString("age_group");
                booking_type.setChecked(jsonObject.getInt("booking_type") == 1);
                termsID = jsonObject.getInt("template_id");
                targetAudience = jsonObject.getInt("target_audiance") - 1;
                if (currentStatus == 4) {
                    saveButton.setVisibility(View.GONE);
                    submitButton.setVisibility(View.GONE);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            getSupportActionBar().setTitle("New Promotion");
        fetchAge();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_OK);
            finish();
            return true;
        } else if (item.getItemId() == R.id.copy) {
            item.setVisible(false);
            promotionID = 0;
            String copy = "Copy " + promo_title.getText().toString();
            promo_title.setText(copy);
            toggleFocus(true);
            getSupportActionBar().setTitle("New Promotion");

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.select_template) {
            Intent intent = new Intent(this, Template_Terms.class);
            startActivityForResult(intent, ACT);
        } else if (id == R.id.save) {
            saveUpdatePromotion(true);
        } else if (id == R.id.from_time || id == R.id.to_time) {
            time(v);
        } else if (id == R.id.from_date || id == R.id.to_date) {
            date(v);
        } else if (id == R.id.submit) {
            saveUpdatePromotion(false);
        } else if (id == R.id.save_template) {
            addTemplate(terms.getText().toString());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (promotionID != 0) {
            getMenuInflater().inflate(R.menu.copy, menu);
            this.menu = menu;
            return true;
        } else
            return super.onCreateOptionsMenu(menu);
    }

    private void fetchLocality() {

        progressBar.setVisibility(View.VISIBLE);
        String URL = NetworkUtility.LIST_LOCALITY;
        Log.e("URL", URL);
        Log.e("Access", sp.getAccessToken());
        JSONObject jsonObject = new JSONObject();
        localityData.clear();


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressBar.setVisibility(View.GONE);
                localityData.clear();

                Log.e("response", String.valueOf(response));
                try {
                    if (response.getString("status").equals("200")) {
                        JSONArray jsonArray = response.getJSONArray("cities");
                        int length = jsonArray.length();
                        for (int i = 0; i < length; i++) {
                            JSONObject eachObject = jsonArray.getJSONObject(i);
                            NameID localityData_ = new NameID();
                            localityData_.setId(eachObject.getInt("id"));
                            localityData_.setName(eachObject.getString("name"));
                            localityData.add(localityData_);
                            if (!localityString.isEmpty()) {
                                String[] contains = localityString.split(",");
                                for (String data : contains
                                        ) {
                                    if (data.equals(String.valueOf(eachObject.getInt("id")))) {
                                        Tag tag = new Tag(eachObject.getString("name"));
                                        tag.setId(eachObject.getInt("id"));
                                        tagView.addTag(tag);
                                    }

                                }
                            }


                        }
                        autoCompleteTextView.setAdapter(new NameIDAdapter(NewPromotion.this, R.layout.simple_text, localityData));
                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                fetchTemplate();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
                progressBar.setVisibility(View.GONE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);


    }


    private void fetchAge() {
        progressBar.setVisibility(View.VISIBLE);
        String URL = NetworkUtility.LIST_AGE;
        Log.e("URL", URL);
        Log.e("Access", sp.getAccessToken());
        JSONObject jsonObject = new JSONObject();
        target.clear();
        ageGroupData.clear();


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressBar.setVisibility(View.GONE);

                Log.e("response", String.valueOf(response));
                try {
                    if (response.getString("status").equals("200")) {


                        JSONArray jsonArray = response.getJSONObject("result").getJSONArray("age_group");
                        int length = jsonArray.length();
                        for (int i = 0; i < length; i++) {
                            JSONObject eachObject = jsonArray.getJSONObject(i);

                            NameID chips = new NameID();
                            chips.setId(eachObject.getInt("age_group_id"));
                            chips.setName(eachObject.getString("age_group_text"));
                            if (!ageString.isEmpty()) {
                                String[] contains = ageString.split(",");
                                for (String data : contains
                                        ) {
                                    if (data.equals(String.valueOf(eachObject.getInt("age_group_id")))) {
                                        Tag tag = new Tag(eachObject.getString("age_group_text"));
                                        tag.setId(eachObject.getInt("age_group_id"));
                                        ageGroup.addTag(tag);
                                    }

                                }
                            }


                            ageGroupData.add(chips);
                        }
                        ageGroupEdit.setAdapter(new NameIDAdapter(NewPromotion.this, R.layout.simple_text, ageGroupData));


                        jsonArray = response.getJSONObject("result").getJSONArray("target_audience");
                        length = jsonArray.length();
                        for (int i = 0; i < length; i++) {
                            JSONObject eachObject = jsonArray.getJSONObject(i);
                            target.add(eachObject.getString("text"));
                        }
                        arrayAdapter.notifyDataSetChanged();
                        audience.setSelection(targetAudience);

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                fetchLocality();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
                progressBar.setVisibility(View.GONE);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }


    public void updateTerms(int id, String termValue) {
        terms.setText(termValue);
        currentTemplate = termValue;
        terms.setTag(id);
    }


    private void fetchTemplate() {

        String URL = NetworkUtility.LIST_TEMPLATE;
        Log.e("URL", URL);
        Log.e("Access", sp.getAccessToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("response", String.valueOf(response));
                try {
                    if (response.getString("status").equals("200")) {
                        JSONArray jsonArray = response.getJSONArray("templates");
                        if (termsID != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if (jsonObject.getInt("id") == termsID) {
                                    terms.setText(jsonObject.getString("description"));
                                    terms.setTag(jsonObject.getInt("id"));
                                    currentTemplate = jsonObject.getString("description");
                                    save_template.setVisibility(View.GONE);
                                }
                            }
                        }


                    } else {


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }

    private void saveUpdatePromotion(final boolean isSave) {
        boolean error = false;

        if (promo_title.getText().toString().isEmpty()) {
            promo_title.setError("title cannot be empty");
            error = true;
        }
        if (from_date.getText().toString().isEmpty()) {
            from_date.setError("from date cannot be empty");
            error = true;

        }
        if (to_date.getText().toString().isEmpty()) {
            to_date.setError("to date cannot be empty");
            error = true;
        }
        if (from_time.getText().toString().isEmpty()) {
            from_time.setError("from time cannot be empty");
            error = true;
        }
        if (to_time.getText().toString().isEmpty()) {
            to_time.setError("to time cannot be empty");
            error = true;
        }
        if (max_coupons.getText().toString().isEmpty()) {
            max_coupons.setError("Max number of coupons should be more than 0");
            error = true;
        }
        if ((ageGroup.getTags()).size() == 0) {
            Toast.makeText(this, "Please select at least one age group", Toast.LENGTH_SHORT).show();
            error = true;
        }
        if ((tagView.getTags()).size() == 0) {
            Toast.makeText(this, "Please select at least one locality", Toast.LENGTH_SHORT).show();
            error = true;
        }
        if (notification_message.getText().toString().isEmpty()) {
            notification_message.setError("Notification message cannot be empty");
            error = true;
        }
        if (desc.getText().toString().isEmpty()) {
            desc.setError("You need to add detailed description");
            error = true;
        }
        if (terms.getTag() == null) {
            terms.setError("Please select a template");
        }

        if (error)
            return;

        String URL = NetworkUtility.SAVE_PROMOTION;
        if (promotionID != 0)
            URL = NetworkUtility.UPDATE_PROMOTION;

        Log.e("URL", URL);
        Log.e("Access", sp.getAccessToken());

        JSONObject jsonObject = new JSONObject();
        try {
            if (promotionID != 0)
                jsonObject.put("id", promotionID);
            jsonObject.put("title", promo_title.getText().toString().trim());
            jsonObject.put("status", isSave ? 1 : 2);
            jsonObject.put("from_date", from_date.getText().toString().trim() + " " + from_time.getText().toString());
            jsonObject.put("to_date", to_date.getText().toString().trim() + " " + to_time.getText().toString());
            jsonObject.put("booking_type", booking_type.isChecked() ? 1 : 0);
            jsonObject.put("max_number_of_coupons", max_coupons.getText().toString());
            JSONArray jsonArray = new JSONArray();
            List<Tag> contactsSelected = ageGroup.getTags();
            for (int i = 0; i < contactsSelected.size(); i++) {
                jsonArray.put(contactsSelected.get(i).id);
            }
            jsonObject.put("age_group", jsonArray);
            jsonObject.put("target_audiance", audience.getSelectedItemPosition() + 1);
            jsonObject.put("notification_message", notification_message.getText().toString());
            jsonObject.put("detailed_description", desc.getText().toString());
            jsonObject.put("template_id", terms.getTag());
            jsonArray = new JSONArray();
            List<Tag> areaSelected = tagView.getTags();
            for (int i = 0; i < areaSelected.size(); i++) {
                jsonArray.put(areaSelected.get(i).id);
            }
            jsonObject.put("locality", jsonArray);
            Log.e("Data", jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        progressBar.setVisibility(View.VISIBLE);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressBar.setVisibility(View.GONE);
                Log.e("response", String.valueOf(response));
                try {


                    String dateDiff = from_date.toString();
                    if (response.getString("status").equals("200")) {
                        try {
                            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(from_date.getText().toString());
                            long time = date1.getTime() - 60 * 60 * 24;
                            date1 = new Date(time);
                            dateDiff = new SimpleDateFormat("yyyy-mm-dd").format(date1);


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        AlertDialog alertDialog = new Alert(NewPromotion.this, (isSave) ? "Promotion is saved as Draft. You will need to submit it to start notifying members" : "Dealster will publish the promotion starting " + from_date.getText().toString() + ". Changes can be made till 11:00 PM " + dateDiff, "OK", (isSave) ? R.mipmap.alert_message : R.mipmap.thumbs_up).getAlertDialog();
                        alertDialog.show();
                        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                setResult(NewPromotion.RESULT_OK);
                                NewPromotion.this.finish();
                            }
                        });
                    } else {
                        SnackMessage("Error : " + response.getString("status"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }


    private void time(final View bview) {

        final Calendar cal = Calendar.getInstance();
        final int mHour = cal.get(Calendar.HOUR_OF_DAY);
        final int mMinute = cal.get(Calendar.MINUTE);

        final TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                Calendar c = Calendar.getInstance();
                long start = cal.getTimeInMillis();
                long end = c.getTimeInMillis();
                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                c.set(Calendar.MINUTE, minute);

//                String mStringGetTime = String.valueOf(DateFormat.format("h:mm aa", c));
                String mStringGetTime = String.valueOf(DateFormat.format("h:mm ss", c));
                ((Button) bview).setText(mStringGetTime);

            }
        }, mHour, mMinute, false);

        timePickerDialog.show();

    }

    private void date(final View bview) {
        final Calendar c = Calendar.getInstance();
        final Calendar from = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        long diff = 0;


        if (!from_date.getText().toString().contains("Date")) {

            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(from_date.getText().toString());
                from.setTime(date1);
                diff = from.getTimeInMillis() - c.getTimeInMillis();


            } catch (ParseException e) {
                e.printStackTrace();
            }


        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String date_time = year + "-" + new DecimalFormat("00").format(monthOfYear + 1) + "-" + new DecimalFormat("00").format(dayOfMonth);
                        ((Button) bview).setText(date_time);


                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis() + diff);// set today's date as min date
        datePickerDialog.show();

    }

    private void SnackMessage(String message) {
        Snackbar snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(getResources().getColor(android.R.color.white));
        snackbar.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == ACT && resultCode == RESULT_OK) {
            Bundle bundle = data.getBundleExtra("data");
            if (bundle != null) {
                updateTerms(bundle.getInt("key"), bundle.getString("value"));
                toggleSaveTempate(false);
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (terms.getTag() != null) {
            if (!currentTemplate.equals(s.toString()))
                save_template.setVisibility(View.VISIBLE);
            else
                save_template.setVisibility(View.GONE);

        } else {
            save_template.setVisibility(View.VISIBLE);
        }

    }


    public void addTemplate(final String template_value) {
        if (template_value.isEmpty()) {
            SnackMessage("Template cannot be empty");
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("template", template_value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        progressBar.setVisibility(View.VISIBLE);
        String URL = NetworkUtility.SAVE_TEMPLATE;
        JsonObjectRequest request = new JsonObjectRequest(URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response1) {
                Log.e("Template", response1.toString());
                progressBar.setVisibility(View.GONE);
                try {
                    if (response1.getInt("status") == 200) {
                        currentTemplate = template_value;
                        terms.setTag(response1.getInt("id"));
                        toggleSaveTempate(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
                progressBar.setVisibility(View.GONE);
                SnackMessage(error.toString());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }

    private void toggleFocus(boolean state) {
        if (!state) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } else {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }

        promo_title.setEnabled(state);
        promo_title.requestFocus();
        max_coupons.setEnabled(state);
        notification_message.setEnabled(state);
        desc.setEnabled(state);
        terms.setEnabled(state);

        ageGroup.clearFocus();
        autoCompleteTextView.clearFocus();
        ageGroup.setActivated(state);
        autoCompleteTextView.setEnabled(state);
        ageGroupEdit.setEnabled(state);
        tagView.setEnabled(state);
        ageGroup.setEnabled(state);


        select_template.setClickable(state);

        from_date.setClickable(state);
        from_time.setClickable(state);
        to_time.setClickable(state);
        to_date.setClickable(state);

        audience.setEnabled(state);
        booking_type.setFocusable(state);
        booking_type.setEnabled(state);

        if (state) {
            saveButton.setVisibility(View.VISIBLE);
            submitButton.setVisibility(View.VISIBLE);
        }
//        if(currentStatus==)


    }


    @Override
    public void onTagDeleted(TagView view, Tag tag, int position) {
        if (!view.isEnabled()) return;
        if (view.getId() == R.id.locality_layout_)
            tagView.remove(position);
        else
            ageGroup.remove(position);

    }

    private void toggleSaveTempate(boolean state) {
        save_template.setVisibility((state) ? View.VISIBLE : View.GONE);
    }
}
