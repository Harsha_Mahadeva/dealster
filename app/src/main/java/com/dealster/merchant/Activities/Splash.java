package com.dealster.merchant.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;

import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.SP;

/**
 * Created by Harsha on 14/10/16.
 */
public class Splash extends AppCompatActivity {

    final int DELAY = 300;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(getBaseContext());

        setContentView(R.layout.splash);
        nextScreen();

    }

    private void nextScreen() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                SP sp = new SP(Splash.this);
//                sp.storeAuthKey("tvgrmwep5sotuik9");
                if (sp.checkLogin()) {
                    if (!sp.isOTPVerified()) {
                        startActivity(new Intent(Splash.this, OTP.class));
                    } else {

                        startActivity(new Intent(Splash.this, Main.class));

                    }

                } else {
                    startActivity(new Intent(Splash.this, Login.class));
                }
                Splash.this.finish();
            }
        }, DELAY);

    }

}
