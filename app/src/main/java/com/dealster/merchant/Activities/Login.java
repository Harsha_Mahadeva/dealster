package com.dealster.merchant.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dealster.merchant.Network.NetworkUtility;
import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.AppController;
import com.dealster.merchant.Utilities.ProgressDialogueUtility;
import com.dealster.merchant.Utilities.SP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Harsha on 03/04/17.
 */

public class Login extends AppCompatActivity {
    EditText userID, password;
    CoordinatorLayout coordinatorLayout;
    ProgressDialogueUtility progressDialogueUtility;
    SP sp;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        init();
        findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

    }

    private void init() {
        sp = new SP(this);
        userID = findViewById(R.id.user_id);
        password = findViewById(R.id.password);
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        progressDialogueUtility = new ProgressDialogueUtility(this);
    }


    private void login() {

        if (userID.getText().toString().trim().length() < 4) {
            userID.setError("User ID is too short");
            return;
        } else if (password.getText().toString().trim().length() < 4) {
            password.setError("Password is too short");
            return;
        }

        progressDialogueUtility.StartProgressNonCancelable();
        String URL = NetworkUtility.LOGIN;
        Log.e("URL", URL);

        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                progressDialogueUtility.progressDialog.cancel();

                Log.e("response", String.valueOf(response1));
                try {
                    JSONObject response = new JSONObject(response1);
                    if (response.getString("status").equals("200") && response.getString("role").equals("network_partner")) {

                        sp.updateOTPstatus(response.getBoolean("otp_verified"));
                        sp.storeUser(response.getString("name"));
                        sp.storeAuthKey(response.getString("authkey"));
                        if (response.getBoolean("otp_verified"))
                            startActivity(new Intent(Login.this, Main.class));
                        else {
                            Intent intent = new Intent(Login.this, Change_Password.class);
                            intent.putExtra("password", password.getText().toString());
                            startActivity(intent);
                            Login.this.finish();
                        }
                    } else {
                        SnackBarMessage(Login.this.getResources().getString(R.string.error_auth));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
                progressDialogueUtility.progressDialog.cancel();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", userID.getText().toString().trim());
                params.put("password", password.getText().toString().trim());
                return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }

    private void SnackBarMessage(String message) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(getResources().getColor(android.R.color.white));
        snackbar.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
