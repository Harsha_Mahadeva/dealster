package com.dealster.merchant.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.dealster.merchant.Dialogs.Alert;
import com.dealster.merchant.Network.NetworkUtility;
import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.AppController;
import com.dealster.merchant.Utilities.ProgressDialogueUtility;
import com.dealster.merchant.Utilities.SP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Harsha on 06/04/17.
 */

public class Account_Manager extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    TextView firstName, lastName, phone, startdate, enddate, email;
    CheckBox _firstName, _lastName, _phone, _startdate, _enddate, _email;
    SP sp;
    CoordinatorLayout coordinatorLayout;
    ProgressDialogueUtility progressDialogueUtility;
    int flags = 0;
    Button btnContinue, btnReport;
    JSONObject OwnerDetails;

    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    boolean first_time = true;
    int offset = 0;
    float den = 1;
    ImageView toolbarImage;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_manager);
        toolbar = findViewById(R.id.toolbar);
        flags = getIntent().getIntExtra("flag", 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        appBarLayout = findViewById(R.id.app_bar);
        collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle("Account Manager");
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        toolbarImage = findViewById(R.id.toolbar_image);
        applyFontToToolbar(collapsingToolbarLayout);
        setScroll();
        init();
        btnContinue = findViewById(R.id.continue_button);
        btnReport = findViewById(R.id.report_button);
        btnReport.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
    }

    private void init() {

        sp = new SP(this);
        firstName = findViewById(R.id.first_name);
        _firstName = findViewById(R.id._first_name);

        lastName = findViewById(R.id.last_name);
        _lastName = findViewById(R.id._last_name);


        phone = findViewById(R.id.phone_number);
        _phone = findViewById(R.id._phone_number);


        startdate = findViewById(R.id.start_date);
        _startdate = findViewById(R.id._start_date);


        enddate = findViewById(R.id.end_date);
        _enddate = findViewById(R.id._end_date);


        email = findViewById(R.id.email);
        _email = findViewById(R.id._email);


        coordinatorLayout = findViewById(R.id.coordinator_layout);
        progressDialogueUtility = new ProgressDialogueUtility(this);
        loadDetails();
    }


    private void fillDetails() {

        try {
            firstName.setText(OwnerDetails.getJSONObject("first_name").getString("name"));
            _firstName.setChecked(OwnerDetails.getJSONObject("first_name").getInt("status") == 1);
            _firstName.setTag("first_name");
            if (OwnerDetails.getJSONObject("first_name").getInt("status") == 0) flags++;


            lastName.setText(OwnerDetails.getJSONObject("last_name").getString("name"));
            _lastName.setChecked(OwnerDetails.getJSONObject("last_name").getInt("status") == 1);
            _lastName.setTag("last_name");
            if (OwnerDetails.getJSONObject("last_name").getInt("status") == 0) flags++;

            phone.setText(OwnerDetails.getJSONObject("phone").getString("name"));
            _phone.setChecked(OwnerDetails.getJSONObject("phone").getInt("status") == 1);
            _phone.setTag("phone");
            if (OwnerDetails.getJSONObject("phone").getInt("status") == 0) flags++;


            startdate.setText(OwnerDetails.getJSONObject("start_date").getString("name"));
            _startdate.setChecked(OwnerDetails.getJSONObject("start_date").getInt("status") == 1);
            _startdate.setTag("start_date");
            if (OwnerDetails.getJSONObject("start_date").getInt("status") == 0) flags++;


            enddate.setText(OwnerDetails.getJSONObject("end_date").getString("name"));
            _enddate.setChecked(OwnerDetails.getJSONObject("end_date").getInt("status") == 1);
            _enddate.setTag("end_date");
            if (OwnerDetails.getJSONObject("end_date").getInt("status") == 0) flags++;

            email.setText(OwnerDetails.getJSONObject("email_id").getString("name"));
            _email.setChecked(OwnerDetails.getJSONObject("email_id").getInt("status") == 1);
            _email.setTag("email_id");
            if (OwnerDetails.getJSONObject("email_id").getInt("status") == 0) flags++;

            _email.setOnCheckedChangeListener(this);
            _enddate.setOnCheckedChangeListener(this);
            _startdate.setOnCheckedChangeListener(this);
            _phone.setOnCheckedChangeListener(this);
            _firstName.setOnCheckedChangeListener(this);
            _lastName.setOnCheckedChangeListener(this);
            toggleButton();


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void loadDetails() {

        progressDialogueUtility.StartProgressNonCancelable();
        String URL = NetworkUtility.MERCHANT_DETAILS;

        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                progressDialogueUtility.progressDialog.cancel();

                try {
                    JSONObject response = new JSONObject(response1);
                    Log.e("Response", String.valueOf(response1));

                    if (response.getString("status").equals("200")) {
                        OwnerDetails = response.getJSONObject("account_manager");
                        fillDetails();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialogueUtility.progressDialog.cancel();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String object = (String) buttonView.getTag();
        Log.e("ObjectAM", String.valueOf(object));
        if (isChecked)
            flags--;
        else flags++;
        try {
            OwnerDetails.getJSONObject(object).put("status", (isChecked) ? 1 : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        toggleButton();
    }


    private void SnackBarMessage(String message) {

        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(getResources().getColor(android.R.color.white));
        snackbar.show();
    }

    @Override
    public void onClick(View v) {
        report();
    }


    private void toggleButton() {
        if (flags > 0) {
            btnContinue.setVisibility(View.GONE);
            btnReport.setVisibility(View.VISIBLE);
        } else {
            btnContinue.setVisibility(View.VISIBLE);
            btnReport.setVisibility(View.GONE);
        }
    }


    private void report() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account_manager", OwnerDetails);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("CCOUBR MANAGER", jsonObject.toString());
        progressDialogueUtility.StartProgressNonCancelable();
        String URL = NetworkUtility.FLAG_ACCOUNT_MANAGER;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response1) {
                Log.e("Response", String.valueOf(response1));
                progressDialogueUtility.progressDialog.cancel();
                if (response1.has("status")) {
                    try {
                        Intent prev = getIntent();
                        int isFlagged = prev.getIntExtra("flag", 0);
                        Log.e("previousFlagAccount", String.valueOf(isFlagged));

                        if (response1.getInt("status") == 200 && isFlagged == 0) {
                            startActivity(new Intent(Account_Manager.this, OTP.class));
                        } else if (isFlagged != 0) {


                            AlertDialog alertDialog = new Alert(Account_Manager.this, "Thank you for reporting errors in the account information. Dealster will be contacting you to resolve these.", "OK", R.mipmap.alert_message).getAlertDialog();
                            alertDialog.show();
                            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    sp.clearData();
                                    startActivity(new Intent(Account_Manager.this, Login.class));
                                }
                            });

                        } else {
                            Toast.makeText(Account_Manager.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
                progressDialogueUtility.progressDialog.cancel();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setScroll() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (first_time) {
                    first_time = false;
                    offset = appBarLayout.getHeight() - toolbar.getHeight();
                    den = offset / 100;
                }
                toolbarImage.setAlpha(1 - (-verticalOffset / (den * 100)));
            }
        });
    }


    private void applyFontToToolbar(CollapsingToolbarLayout toolbar) {

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");
        toolbar.setCollapsedTitleTypeface(font);
        toolbar.setExpandedTitleTypeface(font);

    }


}
