package com.dealster.merchant.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dealster.merchant.Dialogs.Alert;
import com.dealster.merchant.Network.NetworkUtility;
import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.AppController;
import com.dealster.merchant.Utilities.ProgressDialogueUtility;
import com.dealster.merchant.Utilities.SP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Harsha on 03/04/17.
 */

public class Change_Password extends AppCompatActivity {

    EditText current_password, new_password, new_password_repeat;
    ProgressDialogueUtility progressDialogueUtility;
    CoordinatorLayout coordinatorLayout;
    SP sp;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        current_password =  findViewById(R.id.current_password);
        new_password =  findViewById(R.id.new_password);
        new_password_repeat =  findViewById(R.id.new_password_repeat);
        progressDialogueUtility = new ProgressDialogueUtility(this);
        coordinatorLayout =  findViewById(R.id.coordinator_layout);
        sp = new SP(this);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePassword();
            }
        });
    }

    private void changePassword() {

        Intent intent = getIntent();
        String oldpassword = intent.getStringExtra("password");


        if (!current_password.getText().toString().trim().equals(oldpassword)) {
            current_password.setError("Invalid old password");
            return;
        } else if (new_password.getText().toString().trim().length() == 0) {
            new_password.setError("Invalid password");
            return;
        } else if (new_password_repeat.getText().toString().trim().length() == 0) {
            new_password_repeat.setError("Invalid password");
            return;
        }
        if (!new_password_repeat.getText().toString().trim().equals(new_password.getText().toString().trim())) {
            AlertDialog alertDialog = new Alert(Change_Password.this, "New password doesn't match", "OK", R.mipmap.alert_message).getAlertDialog();
            alertDialog.show();
            return;
        }


        progressDialogueUtility.StartProgressNonCancelable();
        String URL = NetworkUtility.CHANGE_PASSWORD;
        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                progressDialogueUtility.progressDialog.cancel();

                Log.e("response", String.valueOf(response1));
                try {
                    JSONObject response = new JSONObject(response1);
                    if (response.getString("status").equals("200")) {
                        startActivity(new Intent(Change_Password.this, Business_Details.class));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
                progressDialogueUtility.progressDialog.cancel();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("current_password", current_password.getText().toString().trim());
                params.put("new_password", new_password.getText().toString().trim());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
