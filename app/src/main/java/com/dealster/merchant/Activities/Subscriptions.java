package com.dealster.merchant.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.dealster.merchant.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by m on 1/26/18.
 */

public class Subscriptions extends AppCompatActivity {
    final int PHONE_CALL = 101;

    //    CoordinatorLayout parent;
    AppBarLayout appBarLayout;
    //    RelativeLayout progress_bar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar toolbar;
    ImageView toolbarImage;
    boolean first_time = true;
    int offset = 0;
    float den = 1;
//    NoInternetView noInternetView;
//    InternetConnectivity internet = new InternetConnectivity(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscription_confirmation);
        InitializeUI();
        setScroll();

    }

    private void InitializeUI() {
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        appBarLayout =  findViewById(R.id.app_bar);
        collapsingToolbarLayout =  findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle("SUBSCRIPTION");
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        toolbarImage =  findViewById(R.id.toolbar_image);

        findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Subscriptions.this, PaymentConfirmation.class));
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_OK);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void SnackBarMessage(String message) {
//        Snackbar snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_SHORT);
//        View sbView = snackbar.getView();
////        sbView.setBackgroundColor(getResources().getColor(R.color.snackbar_background));
//        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        registerReceiver(internet, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));


    }

//    @Override
//    public void ConnectionChanged(Boolean connected) {
////        noInternetView.update(connected);
//    }

    @Override
    protected void onPause() {
        super.onPause();
//        unregisterReceiver(internet);
    }


    private void setScroll() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (first_time) {
                    first_time = false;
                    offset = appBarLayout.getHeight() - toolbar.getHeight();
                    den = offset / 100;
                }
                toolbarImage.setAlpha(1 - (-verticalOffset / (den * 100)));
            }
        });
    }


    private void permission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CALL_PHONE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    PHONE_CALL);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    PHONE_CALL);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
