package com.dealster.merchant.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.dealster.merchant.Network.NetworkUtility;
import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.AppController;
import com.dealster.merchant.Utilities.ProgressDialogueUtility;
import com.dealster.merchant.Utilities.SP;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.CalligraphyUtils;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * Created by Harsha on 06/04/17.
 */

public class Business_Details extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, OnMapReadyCallback {


    TextView business_type, business_name, business_address, business_locality, business_pincode, business_landmark, business_city, business_state;
    CoordinatorLayout coordinatorLayout;
    ProgressDialogueUtility progressDialogueUtility;
    CheckBox _business_type, _business_name, _business_address, _business_locality, _business_pincode, _business_landmark, _business_city, _business_state, _map_state;
    SP sp;
    MapView mapView;
    JSONObject MerchantDetails;
    int flags = 0;
    Button btnContinue, btnReport;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;

    boolean first_time = true;
    int offset = 0;
    float den = 1;
    ImageView toolbarImage;
    Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.business_details);
        mapView = findViewById(R.id.mapView);
        appBarLayout = findViewById(R.id.app_bar);
        toolbar = findViewById(R.id.toolbar);

        Intent intent = getIntent();
        flags = intent.getIntExtra("flag", 0);

        mapView.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle("Business Details");
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        toolbarImage = findViewById(R.id.toolbar_image);
        applyFontToToolbar(collapsingToolbarLayout);
        init();
        setScroll();

    }

    private void init() {
        sp = new SP(this);
        business_type = findViewById(R.id.business_type);
        _business_type = findViewById(R.id._business_type);


        business_name = findViewById(R.id.business_name);
        _business_name = findViewById(R.id._business_name);


        business_address = findViewById(R.id.business_address);
        _business_address = findViewById(R.id._business_address);


        business_locality = findViewById(R.id.business_locality);
        _business_locality = findViewById(R.id._business_locality);


        business_pincode = findViewById(R.id.business_pincode);
        _business_pincode = findViewById(R.id._business_pincode);


        business_landmark = findViewById(R.id.business_landmark);
        _business_landmark = findViewById(R.id._business_landmark);


        business_city = findViewById(R.id.business_city);
        _business_city = findViewById(R.id._business_city);


        business_state = findViewById(R.id.business_state);
        _business_state = findViewById(R.id._business_state);


        _map_state = findViewById(R.id.map_check);


        btnContinue = findViewById(R.id.continue_button);
        btnContinue.setOnClickListener(this);

        btnReport = findViewById(R.id.report_button);
        btnReport.setOnClickListener(this);
        progressDialogueUtility = new ProgressDialogueUtility(this);
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        loadDetails();
    }

    @Override
    public void onClick(View view) {
        report();
    }


    private void fillDetails() {

        try {
            business_type.setText(MerchantDetails.getJSONObject("business_category").getString("name"));
            _business_type.setChecked(MerchantDetails.getJSONObject("business_category").getInt("status") == 1);
            _business_type.setTag("business_category");
            if (MerchantDetails.getJSONObject("business_category").getInt("status") == 0) flags++;


            business_name.setText(MerchantDetails.getJSONObject("trade_name").getString("name"));
            _business_name.setChecked(MerchantDetails.getJSONObject("trade_name").getInt("status") == 1);
            _business_name.setTag("trade_name");
            if (MerchantDetails.getJSONObject("trade_name").getInt("status") == 0) flags++;


            business_address.setText(MerchantDetails.getJSONObject("address").getString("name"));
            _business_address.setChecked(MerchantDetails.getJSONObject("address").getInt("status") == 1);
            _business_address.setTag("address");
            if (MerchantDetails.getJSONObject("address").getInt("status") == 0) flags++;


            business_locality.setText(MerchantDetails.getJSONObject("locality").getString("name"));
            _business_locality.setChecked(MerchantDetails.getJSONObject("locality").getInt("status") == 1);
            _business_locality.setTag("locality");
            if (MerchantDetails.getJSONObject("locality").getInt("status") == 0) flags++;


            business_pincode.setText(MerchantDetails.getJSONObject("pincode").getString("name"));
            _business_pincode.setChecked(MerchantDetails.getJSONObject("pincode").getInt("status") == 1);
            _business_pincode.setTag("pincode");
            if (MerchantDetails.getJSONObject("pincode").getInt("status") == 0) flags++;


            business_landmark.setText(MerchantDetails.getJSONObject("landmark").getString("name"));
            _business_landmark.setChecked(MerchantDetails.getJSONObject("landmark").getInt("status") == 1);
            _business_landmark.setTag("landmark");
            if (MerchantDetails.getJSONObject("landmark").getInt("status") == 0) flags++;


            business_city.setText(MerchantDetails.getJSONObject("city").getString("name"));
            _business_city.setChecked(MerchantDetails.getJSONObject("city").getInt("status") == 1);
            _business_city.setTag("city");
            if (MerchantDetails.getJSONObject("city").getInt("status") == 0) flags++;


            business_state.setText(MerchantDetails.getJSONObject("state").getString("name"));
            _business_state.setChecked(MerchantDetails.getJSONObject("state").getInt("status") == 1);
            _business_state.setTag("state");

            _business_type.setOnCheckedChangeListener(this);
            _business_name.setOnCheckedChangeListener(this);
            _business_address.setOnCheckedChangeListener(this);
            _business_locality.setOnCheckedChangeListener(this);
            _business_pincode.setOnCheckedChangeListener(this);
            _business_landmark.setOnCheckedChangeListener(this);
            _business_city.setOnCheckedChangeListener(this);
            _business_state.setOnCheckedChangeListener(this);
            _map_state.setOnCheckedChangeListener(this);


            if (MerchantDetails.getJSONObject("state").getInt("status") == 0) flags++;

            toggleButton();

            mapView.getMapAsync(this);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void loadDetails() {

        progressDialogueUtility.StartProgressNonCancelable();
        String URL = NetworkUtility.MERCHANT_DETAILS;

        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                progressDialogueUtility.progressDialog.cancel();
                Log.e("response", response1);

                try {
                    JSONObject response = new JSONObject(response1);
                    if (response.getString("status").equals("200")) {

                        MerchantDetails = response.getJSONObject("network_partner");
                        fillDetails();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialogueUtility.progressDialog.cancel();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String object = (String) buttonView.getTag();
        Log.e("ObjectBD", String.valueOf(object));

        if (isChecked)
            flags--;
        else flags++;
        try {
            MerchantDetails.getJSONObject(object).put("status", (isChecked) ? 1 : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        toggleButton();


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            LatLng location = new LatLng(MerchantDetails.getJSONObject("location").getLong("lat"), MerchantDetails.getJSONObject("location").getLong("long"));
            googleMap.addMarker(new MarkerOptions()
                    .position(location));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 14));
            googleMap.getUiSettings().setZoomControlsEnabled(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    private void toggleButton() {
        if (flags > 0) {
            btnContinue.setVisibility(View.GONE);
            btnReport.setVisibility(View.VISIBLE);
        } else {
            btnContinue.setVisibility(View.VISIBLE);
            btnReport.setVisibility(View.GONE);
        }
    }


    private void report() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("network_partner", MerchantDetails);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Suth", sp.getAccessToken());
        Log.e("Output", String.valueOf(jsonObject));
        progressDialogueUtility.StartProgressNonCancelable();
        String URL = NetworkUtility.FLAG_MERCHANT_DETAILS;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response1) {
                Log.e("Response", String.valueOf(response1));
                progressDialogueUtility.progressDialog.cancel();
                if (response1.has("status")) {
                    try {
                        if (response1.getInt("status") == 200 || response1.getInt("status") == 500) {
                            Intent intent = new Intent(Business_Details.this, Owner_Details.class);
                            Log.e("Total Flags", String.valueOf(flags));
                            intent.putExtra("flag", flags);
                            startActivity(intent);
                        } else {
                            Toast.makeText(Business_Details.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
                progressDialogueUtility.progressDialog.cancel();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void setScroll() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (first_time) {
                    first_time = false;
                    offset = appBarLayout.getHeight() - toolbar.getHeight();
                    den = offset / 100;
                }
                toolbarImage.setAlpha(1 - (-verticalOffset / (den * 100)));
            }
        });
    }


    private void applyFontToToolbar(CollapsingToolbarLayout toolbar) {

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");
        toolbar.setCollapsedTitleTypeface(font);
        toolbar.setExpandedTitleTypeface(font);

    }

}
