package com.dealster.merchant.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dealster.merchant.Adapters.SimpleAdapter;
import com.dealster.merchant.Interfaces.OnItemClick;
import com.dealster.merchant.Network.NetworkUtility;
import com.dealster.merchant.R;
import com.dealster.merchant.Utilities.AppController;
import com.dealster.merchant.Utilities.SP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by m on 3/5/18.
 */

public class Template_Terms extends AppCompatActivity implements OnItemClick {
    RecyclerView list;
    SP sp;
    SparseArray<String> templateText = new SparseArray<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        list = (RecyclerView) findViewById(R.id.recycler_view);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        fetchTemplate();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void fetchTemplate() {

        sp = new SP(this);
        String URL = NetworkUtility.LIST_TEMPLATE;
        Log.e("URL", URL);
        Log.e("Access", sp.getAccessToken());
        JSONObject jsonObject = new JSONObject();
        templateText.clear();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("response", String.valueOf(response));
                try {
                    if (response.getString("status").equals("200")) {
                        JSONArray jsonArray = response.getJSONArray("templates");
                        int length = jsonArray.length();
                        for (int i = 0; i < length; i++) {
                            JSONObject eachObject = jsonArray.getJSONObject(i);
                            templateText.append(eachObject.getInt("id"), eachObject.getString("description"));
                        }
                        list.setAdapter(new SimpleAdapter(templateText, Template_Terms.this));
                    } else {


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authkey", sp.getAccessToken());
                params.put("Accept", "application/json");
                params.put("Content-Type", "application/json");
                return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }


    @Override
    public void onClick(Object object) {
        Bundle bundle = (Bundle) object;
        Intent intent = new Intent();
        intent.putExtra("data", bundle);
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
