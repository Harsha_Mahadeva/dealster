package com.dealster.merchant.Models;

public class NameID {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "LocalityData{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;
}
