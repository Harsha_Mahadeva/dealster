package com.dealster.merchant.Models;

/**
 * Created by m on 2/24/18.
 */

public class PromoList {

    int promo_id, bookingType, maxNoOfCoupons;
    String promo_title;
    String promo_desc;
    String from_date;
    String to_date;
    String age_group;
    String notification_message;
    String locality;
    int template_id;
    int user_id;
    String date_time;
    String json;

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    int promo_status;

    public int getPromo_status() {
        return promo_status;
    }

    public void setPromo_status(int promo_status) {
        this.promo_status = promo_status;
    }


    public int getPromo_id() {
        return promo_id;
    }

    public void setPromo_id(int promo_id) {
        this.promo_id = promo_id;
    }

    public int getBookingType() {
        return bookingType;
    }

    public void setBookingType(int bookingType) {
        this.bookingType = bookingType;
    }

    public int getMaxNoOfCoupons() {
        return maxNoOfCoupons;
    }

    public void setMaxNoOfCoupons(int maxNoOfCoupons) {
        this.maxNoOfCoupons = maxNoOfCoupons;
    }

    public String getPromo_title() {
        return promo_title;
    }

    public void setPromo_title(String promo_title) {
        this.promo_title = promo_title;
    }

    public String getPromo_desc() {
        return promo_desc;
    }

    public void setPromo_desc(String promo_desc) {
        this.promo_desc = promo_desc;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getAge_group() {
        return age_group;
    }

    public void setAge_group(String age_group) {
        this.age_group = age_group;
    }

    public String getNotification_message() {
        return notification_message;
    }

    public void setNotification_message(String notification_message) {
        this.notification_message = notification_message;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public int getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(int template_id) {
        this.template_id = template_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }


}
