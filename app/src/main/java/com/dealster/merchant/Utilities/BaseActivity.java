package com.dealster.merchant.Utilities;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.dealster.merchant.Interfaces.Internet;
import com.dealster.merchant.Network.InternetConnectivity;
import com.dealster.merchant.Network.NoInternetView;

import io.reactivex.disposables.CompositeDisposable;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity implements Internet {

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    ProgressDialogueUtility progressDialogueUtility;
    SP sp;
    View root = null;
    InternetConnectivity internetConnectivity = new InternetConnectivity(this);
    NoInternetView noInternetView;
    private String TAG = BaseActivity.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        noInternetView = new NoInternetView(this);
        progressDialogueUtility = new ProgressDialogueUtility(this);
        sp = new SP(this);
    }

    private void setParent(View parent) {
        this.root = parent;
    }


    private void SnackBarMessage(String message) {
        if (root != null) {
            Snackbar snackbar = Snackbar.make(root, message, Snackbar.LENGTH_SHORT);
            snackbar.getView().setBackgroundColor(getResources().getColor(android.R.color.white));
            snackbar.show();
        }

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void ConnectionChanged(Boolean connected) {
        noInternetView.update(connected);
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(internetConnectivity, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(internetConnectivity);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        noInternetView.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    public void setPageTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }
}
