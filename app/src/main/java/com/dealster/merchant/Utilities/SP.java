package com.dealster.merchant.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.dealster.merchant.R;

/**
 * Created by Harsha on 08/04/17.
 */

public class SP {
    private SharedPreferences sharedPreferences;

    public SP(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.shared_preference), Context.MODE_PRIVATE);
    }


    public void storeAuthKey(String access_token) {

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("access_token", access_token);
        edit.apply();
    }


    public void storeUser(String name) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("name", name);
        edit.apply();
    }

    public String getAccessToken() {
        return sharedPreferences.getString("access_token", null);
    }

    public String getExpiry() {
        return sharedPreferences.getString("expiry", null);
    }

    public String getTokenType() {
        return sharedPreferences.getString("token_type", null);
    }

    public String getUID() {
        return sharedPreferences.getString("uid", null);
    }

    public String getClient() {
        return sharedPreferences.getString("client", null);
    }

    public String getName() {
        return sharedPreferences.getString("name", null);
    }

    public String getUserType() {
        return sharedPreferences.getString("usertype", null);
    }

    public void setUserType(String userType) {

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("usertype", userType);
        edit.apply();
    }

    public void clearData() {

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.clear();
        edit.apply();
        edit.commit();

    }

    public boolean checkLogin() {
        return sharedPreferences.getString("access_token", null) != null;
    }

    public boolean isOTPVerified() {
        return sharedPreferences.getBoolean("otp_verified", false);
    }

    public boolean isDetailsVerified() {
        return sharedPreferences.getBoolean("details_verified", false);
    }

    public void updateOTPstatus(boolean state) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("otp_verified", state);
        edit.apply();
    }

    public void updateDetailsStatus(boolean state) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("details_verified", state);
        edit.apply();
    }

    public void storeEmail(String email) {

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("email", email);
        edit.apply();
    }

    public String getEmail() {
        return sharedPreferences.getString("email", null);
    }

    public void StoreOTP(String otp) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("otp", otp);
        edit.apply();
    }

    public String getOTP() {
        return sharedPreferences.getString("otp", null);
    }

    public void StorePhone(String otp) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("phone", otp);
        edit.apply();
    }

    public String getPhone() {
        return sharedPreferences.getString("phone", null);
    }

    public void setNetworkPartnerDetails(String string) {

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("network_partner", string);
        edit.apply();

    }

    public String getNetworkPartnerDetails() {
        return sharedPreferences.getString("network_partner", null);
    }
}
