package com.dealster.merchant.Views;

import android.content.Context;
import android.content.res.Resources;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dealster.merchant.R;

public class nameTitleChecked extends LinearLayout {

    CheckBox checkBox;
    TextView title, Value;


    public nameTitleChecked(Context context) {
        super(context);
        Resources resources = context.getResources();
        setOrientation(HORIZONTAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        setLayoutParams(layoutParams);
        LinearLayout linearLayout = new LinearLayout(context);
        layoutParams = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, .9f);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(VERTICAL);

        title = new TextView(context);
        layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, (int) resources.getDimension(R.dimen.Margin_Small));
        title.setLayoutParams(layoutParams);
        title.setTextColor(resources.getColor(R.color.text_light_grey));
        linearLayout.addView(title);
        Value = new TextView(context);
        final float scale = getContext().getResources().getDisplayMetrics().density;
        int pixels = (int) (50 * scale + 0.5f);
        layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, pixels);
        Value.setBackgroundColor(resources.getColor(android.R.color.white));
        Value.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.START);
        pixels = (int) (16 * scale + 0.5f);
        layoutParams.setMargins(0, 0, 0, pixels);
        Value.setTextColor(resources.getColor(R.color.text_light_grey));
        Value.setLayoutParams(layoutParams);
        linearLayout.addView(Value);
        addView(linearLayout);

    }


    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setValue(String text) {
        Value.setText(text);
    }


}
